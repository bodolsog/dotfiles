#: -----------------------------------------------------------
#: Main
#: -----------------------------------------------------------
# font
font pango:Source Code Pro 10

# mod
set $Mod Mod4

# direction keys
set $left  h
set $down  j
set $up    k
set $right l


set $WS1 "1: 1"
set $WS2 "2: 2"
set $WS3 "3: 3"
set $WS4 "4: 4"
set $WS5 "5: 5"
set $WS6 "6: 6"
set $WS7 "7: 7"
set $WS8 "8: 8"

# monitors
set $Mon1 DisplayPort-0
set $Mon2 DisplayPort-1

#:-----------------------------------------------------------
#: Autostart
#: -----------------------------------------------------------

# set wallpaper
exec_always --no-startup-id nitrogen --restore

# window decorations
exec --no-startup-id picom -b

# polybar
exec_always --no-startup-id killall -q
exec_always --no-startup-id $HOME/.config/polybar/launch.sh

# ulanucher
exec --no-startup-id ulauncher --no-window-shadow 


#: -----------------------------------------------------------
#: Bindings
#: -----------------------------------------------------------

# start a terminal
bindsym $Mod+Return exec alacritty

# kill focused window
bindsym $Mod+Shift+q kill

# screenshot
bindsym $Mod+Ctrl+Print exec --no-startup-id "~/.local/bin/take-screenshot -a"
bindsym $Mod+Shift+Print exec --no-startup-id "~/.local/bin/take-screenshot -w"

# audio
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous
bindsym XF86AudioStop exec --no-startup-id playerctl stop
bindsym XF86AudioRaiseVolume exec --no-startup-id pamixer -i 2
bindsym XF86AudioLowerVolume exec --no-startup-id pamixer -d 2
bindsym XF86AudioMute exec --no-startup-id pamixer -t

# backlight
bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight -inc 10
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 10

# scratpad
bindsym $Mod+Shift+grave move scratchpad
bindsym $Mod+grave scratchpad show

# programs
bindsym Ctrl+grave exec --no-startup-id ulauncher-toggle

bindsym $Mod+F1 exec --no-startup-id nautilus

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $Mod+Shift+r restart

# reload the configuration file
bindsym $Mod+Shift+c reload


#: -----------------------------------------------------------
#: Workspace
#: -----------------------------------------------------------

# switch to workspace
bindsym $Mod+1 workspace $ws1
bindsym $Mod+2 workspace $ws2
bindsym $Mod+3 workspace $ws3
bindsym $Mod+4 workspace $ws4
bindsym $Mod+5 workspace $ws5
bindsym $Mod+6 workspace $ws6
bindsym $Mod+7 workspace $ws7
bindsym $Mod+8 workspace $ws8

# move focused container to workspace
bindsym $Mod+Shift+1 move container to workspace $ws1
bindsym $Mod+Shift+2 move container to workspace $ws2
bindsym $Mod+Shift+3 move container to workspace $ws3
bindsym $Mod+Shift+4 move container to workspace $ws4
bindsym $Mod+Shift+5 move container to workspace $ws5
bindsym $Mod+Shift+6 move container to workspace $ws6
bindsym $Mod+Shift+7 move container to workspace $ws7
bindsym $Mod+Shift+8 move container to workspace $ws8

# move workspace
bindsym $Mod+Shift+Right move workspace to output right
bindsym $Mod+Shift+Left  move workspace to output left


#: -----------------------------------------------------------
#: Gaps
#: -----------------------------------------------------------
for_window [class="^.*"] border none, title_format " %title "

set  $gOuter      0
set  $gInner      5
set  $gHorizontal 120
set  $gVertical   75
gaps outer        $gOuter
gaps inner        $gInner
gaps horizontal   $gHorizontal
gaps vertical     $gVertical

default_border          pixel 1
new_window              normal
new_float               normal
hide_edge_borders       both
popup_during_fullscreen smart

# changing border style
bindsym $Mod+n border normal
bindsym $Mod+y border 1pixel
bindsym $Mod+u border none

# change gaps
bindsym $Mod+9                   gaps horizontal current set 0; gaps vertical current set 0
bindsym $Mod+0                   gaps horizontal current set $gHorizontal; gaps vertical current set $gVertical
bindsym $Mod+Shift+0             gaps horizontal current set 5; gaps vertical current set $gVertical
bindsym $Mod+plus                gaps inner current plus  5
bindsym $Mod+minus               gaps inner current minus 5
bindsym $Mod+Shift+plus          gaps outer current plus  5
bindsym $Mod+Shift+minus         gaps outer current minus 5
bindsym $Mod+Control+plus        gaps inner all plus      5
bindsym $Mod+Control+minus       gaps inner all minus     5
bindsym $Mod+Control+Shift+plus  gaps outer all plus      5
bindsym $Mod+Control+Shift+minus gaps outer all minus     5



#:-----------------------------------------------------------
#: Container/Window config
#:-----------------------------------------------------------

# Scratchpad, Floating
bindsym $Mod+Shift+space floating toggle
floating_modifier  Mod1

# change focus
bindsym $Mod+j         focus left
bindsym $Mod+k         focus down
bindsym $Mod+l         focus up
bindsym $Mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $Mod+Left  focus left
bindsym $Mod+Down  focus down
bindsym $Mod+Up    focus up
bindsym $Mod+Right focus right

# move focused window
bindsym $Mod+Shift+h move left
bindsym $Mod+Shift+j move down
bindsym $Mod+Shift+k move up
bindsym $Mod+Shift+l move right

# split in horizontal orientation
#bindsym $Mod+h split h

# split in vertical orientation
#bindsym $Mod+v split v

# enter fullscreen mode for the focused container
bindsym $Mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $Mod+s layout stacked
bindsym $Mod+t layout tabbed
bindsym $Mod+e layout toggle split

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# Size
bindsym Mod1+Up    resize shrink height 10 px or 1 ppt
bindsym Mod1+Down  resize grow   height 10 px or 1 ppt
bindsym Mod1+Left  resize shrink width  10 px or 1 ppt
bindsym Mod1+Right resize grow   width  10 px or 1 ppt


#:-----------------------------------------------------------
#: Applications
#: -----------------------------------------------------------
for_window [class="(?i)(?:pinentry)"]                floating enable, focus
for_window [class="(?i)(?:thunar)"]                  focus
for_window [class="(?i)(?:termite)"]                 floating enable, focus, resize set width 900 height 500
for_window [class="(?i)(?:jetbrains-pycharm)"]       focus, gaps outer current set 0, gaps inner current set 3
for_window [title="(?i)(?:copying|deleting|moving)"] floating enable
for_window [window_role="(?i)(?:pop-up|setup)"]      floating enable
for_window [window_role="(?i)(?:status-window)"]     floating enable, border normal
for_window [class="(?i)(?:gnome-calculator)"]        floating enable, focus
for_window [title="(?i)(?:Android Emulator(.*?)"]    floating enable
for_window [title="Emulator"]                        floating enable
for_window [class="(?i)(?:ulauncher)"]		     floating enable

