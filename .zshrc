ZSH_THEME="bodolsog"

ZSH="$HOME/.oh-my-zsh"
ZSH_CUSTOM="$HOME/.zsh_custom"

source $ZSH_CUSTOM/plugins
source $ZSH/oh-my-zsh.sh


HISTFILE=~/.zsh_histfile
HISTSIZE=1000
SAVEHIST=5000
setopt nomatch notify
unsetopt autocd beep extendedglob
bindkey -v
zstyle :compinstall filename '/home/pawelbbdrozd/.zshrc'

autoload -Uz compinit
compinit

EDITOR=vim
